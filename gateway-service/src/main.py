import os
from fastapi import FastAPI, File, UploadFile
from fastapi.responses import HTMLResponse

ML_SERVICE_URL = os.environ.get("ML_SERVICE_URL")
PREDICTION_ENDPOINT = f"{ML_SERVICE_URL}/predictions"

app = FastAPI()


@app.get("/", response_class=HTMLResponse)
async def main():
    return """
    <body>
        <form action=""" + f"\"{PREDICTION_ENDPOINT}\"" + """ enctype="multipart/form-data" method="post">
            <input name="image" type="file" multiple>
            <input type="submit">
        </form>
    </body>
    """
