import cv2
import numpy as np
import joblib

from fastapi import FastAPI, File, UploadFile

MODEL_PATH = "resources/models/model_clf.sav"
loaded_model = joblib.load(MODEL_PATH)

app = FastAPI()


@app.post("/predictions")
async def create_upload_files(image: UploadFile = File(...)):

    contents = await image.read()
    np_arr = np.fromstring(contents, np.uint8)

    img = cv2.imdecode(np_arr, cv2.IMREAD_GRAYSCALE)
    res = cv2.resize(img, dsize=(28, 28), interpolation=cv2.INTER_AREA)
    res = ~res
    return {"classification_class": loaded_model.predict(res.reshape(1, -1))[0],
            "confidence": loaded_model.predict_proba(res.reshape(1, -1)).tolist()}


