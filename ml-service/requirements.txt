#Fastapi requirements
fastapi
uvicorn
python-multipart #requirement for UploadFile

#ML requirements
opencv-python-headless
numpy
scikit-learn
