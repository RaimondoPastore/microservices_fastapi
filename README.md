### Playing around with ML and FastApi - WIP

Implementing 2 services: one should serve stati pages to upload images and a service to classify if an image is an apple or not.
For the ML part See https://gitlab.com/RaimondoPastore/ml_python_2020/-/blob/master/Classifiy%20a%20hand-drawn%20apple.ipynb 

`docker-compose up --build`

go to `localhost:5001` and upload an image
